package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Visitante;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.VisitanteRepository;
import br.com.js.igrejavisitanteapi.service.api.VisitanteService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class VisitanteServiceImpl implements VisitanteService {

	@Autowired
	private VisitanteRepository repository;

	@Override
	public Visitante salvar(Visitante entity) {
		return repository.save(entity);
	}

	@Override
	public Visitante atualizar(Long codigo, Visitante entity) {
		Visitante visitanteAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, visitanteAtualiza, "codigo");
		return salvar(visitanteAtualiza);
	}

	@Override
	public Visitante buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Visitante> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

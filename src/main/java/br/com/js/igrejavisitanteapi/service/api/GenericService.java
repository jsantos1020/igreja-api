package br.com.js.igrejavisitanteapi.service.api;

import java.util.Collection;

public interface GenericService<T> {
	T salvar(T entity);
	T atualizar(Long codigo, T entity);
	T buscarPeloCodigo(Long codigo) throws Exception;
	Collection<T> buscarTodos();
	void apagarPeloCodigo(Long codigo);
}

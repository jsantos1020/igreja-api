package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Contato;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.ContatoRepository;
import br.com.js.igrejavisitanteapi.service.api.ContatoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ContatoServiceImpl implements ContatoService {

	@Autowired
	private ContatoRepository repository;

	@Override
	public Contato salvar(Contato entity) {
		return repository.save(entity);
	}

	@Override
	public Contato atualizar(Long codigo, Contato entity) {
		Contato contatoAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, contatoAtualiza, "codigo");
		return salvar(contatoAtualiza);
	}

	@Override
	public Contato buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Contato> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

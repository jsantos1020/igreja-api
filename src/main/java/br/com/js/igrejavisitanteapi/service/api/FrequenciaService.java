package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Frequencia;

public interface FrequenciaService extends GenericService<Frequencia> {
}

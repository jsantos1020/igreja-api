package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Visitante;

public interface VisitanteService extends GenericService<Visitante> {
}

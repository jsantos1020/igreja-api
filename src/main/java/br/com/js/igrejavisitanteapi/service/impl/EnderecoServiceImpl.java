package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Endereco;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.EnderecoRepository;
import br.com.js.igrejavisitanteapi.service.api.EnderecoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class EnderecoServiceImpl implements EnderecoService {

	@Autowired
	private EnderecoRepository repository;

	@Override
	public Endereco salvar(Endereco entity) {
		return repository.save(entity);
	}

	@Override
	public Endereco atualizar(Long codigo, Endereco entity) {
		Endereco enderecoAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, enderecoAtualiza, "codigo");
		return salvar(enderecoAtualiza);
	}

	@Override
	public Endereco buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Endereco> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Cultos;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.CultosRepository;
import br.com.js.igrejavisitanteapi.service.api.CultosService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CultosServiceImpl implements CultosService {

	@Autowired
	private CultosRepository repository;

	@Override
	public Cultos salvar(Cultos entity) {
		return repository.save(entity);
	}

	@Override
	public Cultos atualizar(Long codigo, Cultos entity) {
		Cultos cultosAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, cultosAtualiza, "codigo");
		return salvar(cultosAtualiza);
	}

	@Override
	public Cultos buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Cultos> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

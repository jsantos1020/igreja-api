package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Comunicacao;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.ComunicacaoRepository;
import br.com.js.igrejavisitanteapi.service.api.ComunicacoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class ComunicacaoServiceImpl implements ComunicacoService {

	@Autowired
	private ComunicacaoRepository repository;

	@Override
	@Transactional
	public Comunicacao salvar(Comunicacao entity) {
		return repository.save(entity);
	}

	@Override
	public Comunicacao atualizar(Long codigo, Comunicacao entity) {
		Comunicacao comunicacaoAtualizar = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, comunicacaoAtualizar, "codigo");
		return salvar(comunicacaoAtualizar);
	}

	@Override
	public Comunicacao buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Comunicacao> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

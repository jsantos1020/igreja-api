package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.TipoContato;

public interface TipoContatoService extends GenericService<TipoContato> {
}

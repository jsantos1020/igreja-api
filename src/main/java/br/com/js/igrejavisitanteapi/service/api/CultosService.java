package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Cultos;

public interface CultosService extends GenericService<Cultos> {
}

package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Comunicacao;

public interface ComunicacoService extends GenericService<Comunicacao> {
}

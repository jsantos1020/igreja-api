package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Endereco;

public interface EnderecoService extends GenericService<Endereco> {
}

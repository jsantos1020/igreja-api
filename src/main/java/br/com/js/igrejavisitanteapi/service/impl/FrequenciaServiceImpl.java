package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Frequencia;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.FrequenciaRepository;
import br.com.js.igrejavisitanteapi.service.api.FrequenciaService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class FrequenciaServiceImpl implements FrequenciaService {

	private FrequenciaRepository repository;

	@Override
	public Frequencia salvar(Frequencia entity) {
		return repository.save(entity);
	}

	@Override
	public Frequencia atualizar(Long codigo, Frequencia entity) {
		Frequencia frequenciaAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, frequenciaAtualiza, "codigo");
		return salvar(frequenciaAtualiza);
	}

	@Override
	public Frequencia buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Frequencia> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

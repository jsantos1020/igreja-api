package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.Periodo;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.PeriodoRepository;
import br.com.js.igrejavisitanteapi.service.api.PeriodoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class PeriodoServiceImpl implements PeriodoService {

	@Autowired
	private PeriodoRepository repository;

	@Override
	public Periodo salvar(Periodo entity) {
		return repository.save(entity);
	}

	@Override
	public Periodo atualizar(Long codigo, Periodo entity) {
		Periodo periodoAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, periodoAtualiza, "codigo");
		return salvar(periodoAtualiza);
	}

	@Override
	public Periodo buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<Periodo> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Periodo;

public interface PeriodoService extends GenericService<Periodo> {
}

package br.com.js.igrejavisitanteapi.service.api;

import br.com.js.igrejavisitanteapi.entity.Contato;

public interface ContatoService extends GenericService<Contato> {
}

package br.com.js.igrejavisitanteapi.service.impl;

import br.com.js.igrejavisitanteapi.entity.TipoContato;
import br.com.js.igrejavisitanteapi.exception.VisitanteApiException;
import br.com.js.igrejavisitanteapi.repository.TipoContatoRepository;
import br.com.js.igrejavisitanteapi.service.api.TipoContatoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class TipoContatoServiceImpl implements TipoContatoService {

	@Autowired
	private TipoContatoRepository repository;

	@Override
	public TipoContato salvar(TipoContato entity) {
		return repository.save(entity);
	}

	@Override
	public TipoContato atualizar(Long codigo, TipoContato entity) {
		TipoContato tipoContatoAtualiza = buscarPeloCodigo(codigo);
		BeanUtils.copyProperties(entity, tipoContatoAtualiza, "codigo");
		return salvar(tipoContatoAtualiza);
	}

	@Override
	public TipoContato buscarPeloCodigo(Long codigo) throws VisitanteApiException {
		return repository.findById(codigo).orElseThrow(VisitanteApiException::new);
	}

	@Override
	public Collection<TipoContato> buscarTodos() {
		return repository.findAll();
	}

	@Override
	public void apagarPeloCodigo(Long codigo) {
		repository.deleteById(codigo);
	}
}

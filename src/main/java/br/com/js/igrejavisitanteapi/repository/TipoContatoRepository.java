package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.TipoContato;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends JpaRepository<TipoContato, Long> {
}

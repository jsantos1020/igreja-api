package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.Cultos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CultosRepository extends JpaRepository<Cultos, Long> {
}

package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.Comunicacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComunicacaoRepository extends JpaRepository<Comunicacao, Long> {
}

package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.Periodo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PeriodoRepository extends JpaRepository<Periodo, Long> {
}

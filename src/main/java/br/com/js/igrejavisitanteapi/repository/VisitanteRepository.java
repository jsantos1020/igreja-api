package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.Visitante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitanteRepository extends JpaRepository<Visitante, Long> {
}

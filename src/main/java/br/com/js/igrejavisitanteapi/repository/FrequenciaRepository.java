package br.com.js.igrejavisitanteapi.repository;

import br.com.js.igrejavisitanteapi.entity.Frequencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FrequenciaRepository extends JpaRepository<Frequencia, Long> {
}

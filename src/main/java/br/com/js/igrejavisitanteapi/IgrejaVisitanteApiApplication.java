package br.com.js.igrejavisitanteapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IgrejaVisitanteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IgrejaVisitanteApiApplication.class, args);
	}

}

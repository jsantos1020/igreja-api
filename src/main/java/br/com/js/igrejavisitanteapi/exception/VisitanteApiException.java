package br.com.js.igrejavisitanteapi.exception;

public class VisitanteApiException extends RuntimeException {

	public VisitanteApiException() {
		super("Não possível localizar a informação");
	}
}

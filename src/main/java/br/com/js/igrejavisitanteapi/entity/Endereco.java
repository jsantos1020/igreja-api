package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VENDERECO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Endereco {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDENDERECO")
	private Long codigo;
	@Column(name = "CEP")
	private String cep;
	@Column(name = "LOGRADOURO")
	private String logradouro;
	@Column(name = "COMPLEMENTO")
	private String complemento;
	@Column(name = "BAIRRO")
	private String bairro;
	@Column(name = "LOCALIDADE")
	private String localidade;
	@Column(name = "UF")
	private String uf;
	@OneToOne
	@JoinColumn(name = "IDVISITANTE")
	private Visitante visitante;
}
package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VCOMUNICACAO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Comunicacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDCOMUNICACAO")
	private Long codigo;
	@Column(name = "COMUNICACAO")
	private String descricao;
}

package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VFREQUENCIA")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Frequencia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDFREQUENCIA")
	private Long codigo;
	@Column(name = "FREQUENCIA")
	private String descricao;
}
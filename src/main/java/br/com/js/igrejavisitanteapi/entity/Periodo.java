package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VPERIODO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Periodo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDPERIODO")
	private Long codigo;
	@Column(name = "PERIODO")
	private String descricao;
}
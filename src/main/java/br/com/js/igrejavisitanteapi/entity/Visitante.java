package br.com.js.igrejavisitanteapi.entity;

import br.com.js.igrejavisitanteapi.enumeration.EstadoCivil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;

@Entity
@Table(name = "VISTANTE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Visitante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDVISITANTE")
	private Long codigo;
	@Column(name = "DATAVISITA")
	private LocalDate dataVisita;
	@Column(name = "NOMEVISITANTE")
	private String visitante;
	@Column(name = "DATANASCIMENTO")
	private LocalDate dataNascimento;
	@Column(name = "DESEJAVISITAIGREJA")
	private Boolean desajaVisita;
	@ManyToOne
	@JoinColumn(name = "IDCULTOS")
	private Cultos cultos;
	@Enumerated(EnumType.STRING)
	private EstadoCivil estadoCivil;
	@ManyToOne
	@JoinColumn(name = "IDFREQUENCIA")
	private Frequencia frequencia;
	@ManyToOne
	@JoinColumn(name = "IDCOMUNICACAO")
	private Comunicacao comunicacao;
	@ManyToOne
	@JoinColumn(name = "IDPERIODO")
	private Periodo periodo;
	@OneToMany(mappedBy = "visitante")
	private Collection<Contato> contatos;
}
package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VTIPOCONTATO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TipoContato {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDTIPOCONTATO")
	private Long codigo;
	@Column(name = "TIPOCONTATO")
	private String descricao;
}

package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VCONTATO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contato {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDCONTATO")
	private Long codigo;
	@Column(name = "CONTATO")
	private String descricao;
	@ManyToOne
	@JoinColumn(name = "IDTIPOCONTATO")
	private TipoContato tipoContato;
	@ManyToOne
	@JoinColumn(name = "IDVISITANTE")
	private Visitante visitante;
}
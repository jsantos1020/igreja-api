package br.com.js.igrejavisitanteapi.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "VCULTOS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cultos {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDCULTO")
	private Long codigo;
	@Column(name = "CULTO")
	private String descricao;
}

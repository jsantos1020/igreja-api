package br.com.js.igrejavisitanteapi.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

public abstract class GenericController<E, S> implements Serializable {

	@Autowired
	private ApplicationEventPublisher publisher;
	@Autowired
	private MessageSource messageSource;
	protected S service;

	public GenericController() {
	}

	protected ResponseEntity<E> create(E entity, Long id, HttpServletResponse response) {
		this.publisher.publishEvent(new CreatedResourceEvent(this, response, id));
		return ResponseEntity.status(HttpStatus.CREATED).body(entity);
	}

	protected ResponseEntity<E> update(E e) {
		return ResponseEntity.ok(e);
	}
}

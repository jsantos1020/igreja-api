package br.com.js.igrejavisitanteapi.controller.rest;

import br.com.js.igrejavisitanteapi.controller.config.GenericController;
import br.com.js.igrejavisitanteapi.entity.Comunicacao;
import br.com.js.igrejavisitanteapi.service.api.ComunicacoService;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@RestController
@RequestMapping("/api")
public class ComunicacaoControllerRest extends GenericController<Comunicacao, ComunicacoService> {

	private static final String URI_COMUNICACAO = "/comunicacaos";
	private static final String URI_COMUNNICACAO_ID = URI_COMUNICACAO + "/{codigo}";

	public ComunicacaoControllerRest(ComunicacoService service) {
		this.service = service;
	}

	@PostMapping(URI_COMUNICACAO)
	public ResponseEntity<Comunicacao> salvar(@RequestBody Comunicacao entity, HttpServletResponse response) {
		Comunicacao comunicacao = service.salvar(entity);
		return super.create(comunicacao, comunicacao.getCodigo(), response);
	}

	@PutMapping(URI_COMUNNICACAO_ID)
	public ResponseEntity<Comunicacao> atualizar(@PathVariable Long codigo, @RequestBody Comunicacao comunicacao) {
		return super.update(service.atualizar(codigo, comunicacao));
	}

	@GetMapping
	public ResponseEntity<Collection<Comunicacao>> buscarTodos() {
		return ResponseEntity.ok(service.buscarTodos());
	}

	@GetMapping(URI_COMUNNICACAO_ID)
	@SneakyThrows
	public ResponseEntity<Comunicacao> buscarPeloCodigo(@PathVariable Long codigo) {
		return ResponseEntity.ok(service.buscarPeloCodigo(codigo));
	}

	@DeleteMapping(URI_COMUNNICACAO_ID)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void apagarRegistro(@PathVariable Long codigo) {
		service.apagarPeloCodigo(codigo);
	}
}
package br.com.js.igrejavisitanteapi.controller.config;

import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletResponse;

public class CreatedResourceEvent extends ApplicationEvent {

	private static final long serialVersionUID = 4465252798431785905L;
	private HttpServletResponse response;
	private Long codigo;

	public CreatedResourceEvent(Object source, HttpServletResponse response, Long codigo) {
		super(source);
		this.response = response;
		this.codigo = codigo;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public Long getCodigo() {
		return codigo;
	}
}
